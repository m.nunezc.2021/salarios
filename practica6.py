import unittest
class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
    def calcula_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}". format(name=self.nombre, tax=self.calcula_impuestos())

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.nomina, 5000)
    def test_impuestos(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.calcula_impuestos(), 1500)
    def test_str(self):
        el = Empleado ("Pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", el.__str__())
if __name__ == "__main__":
    unittest.main()
    