import unittest
from empleado import Empleado


class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.nomina, 5000)
    def test_impuestos(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.calcula_impuestos(), 1500)
    def test_str(self):
        el = Empleado ("Pepe", 50000)
        self.assertEqual("El empleado Pepe debe pagar 15000.00", el.__str__())
if __name__ == "__main__":
    unittest.main()
