class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
    def CalculoImpuestos (self):
        impuestos = self.nomina*0.30
        return impuestos
    def imprime(self):
        tmp = self.calculo_impuestos()
        print("El empleado {name} debe pagar {tax:.2f}". format(name=self.nombre, tax=tmp))

empleadoPepe = Empleado("Pepe", 20000)
empleadoAna = Empleado("Ana", 300000)
total = empleadoPepe.calculo_impuestos() + empleadoAna.calculo_impuestos()

empleadoPepe.imprime()
empleadoAna.imprime()
print("Los impuestos a pagar en total son {:.2f} euros".format(total))